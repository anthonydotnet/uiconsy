﻿namespace uIconsy.Controllers.WebApi
{
    using System.IO;
    using System.Web.Hosting;

    using Umbraco.Web.WebApi;

    using System.Collections.Generic;
    using System.Linq;

    public class uIconsyBackofficeController : UmbracoApiController
    {
        public IEnumerable<string> GetIcons(int pageNo)
        {
            if (this.Security.GetUserId() == -1)
            {
                // user not logged in!
                return null;
            }

            var absolutePath = HostingEnvironment.MapPath("/umbraco/images/umbraco/");

            var files = Directory.GetFiles(absolutePath, "*", SearchOption.TopDirectoryOnly);

            var filenames = files.Skip(pageNo * 100).Take(100).Select(Path.GetFileName);

            return filenames;
        } 
    }
}